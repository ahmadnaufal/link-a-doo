$(document).ready(function() {
	var text = ["Doodoos", "Friends", "Partners"];
	var counter = 1;
	setInterval(function() {
		$('#change').fadeOut(500, function() {
			var $this = $(this);
			$this.text(text[counter]);
			counter++;
			counter = counter % text.length;
			$this.fadeIn(500);
		});
	}, 3000);

	var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);

    $(".nav a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            $(this).closest('li').addClass('active');
        }
    });

});