<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The main class of Admin controller
* User register, registration form and also validations and inserting users
*/
class Admin extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		if (($sess = $this->session->userdata('loggedin')) == NULL) {
			// if session is not set, redirect to login page
			$this->session->set_flashdata('flash_data', 'Please login first to access this page!');
			redirect('login');
		}

		/* A condition if user logged in and accessing this page */
		if ($sess['is_privileged'] == 1)
			redirect('/');

		$this->load->model('user_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data['title'] = "Welcome to Admin Page!";
		$data['content'] = $this->user_model->getAllUsers();
		
		// load the default view of register page
		$this->load->view('admin/templates/header');
		$this->load->view('admin/index', $data);
		$this->load->view('templates/footer');
	}

	public function search()
	{
		// get the query from the post if the query is more than one char
		if (strlen($query = $this->input->get('query')) < 2)
			redirect('/');

		$data['title'] = "Search Results for '".$query."'";
		$data['content'] = $this->user_model->searchUserAlt($query);

		// load the search results for 
		$this->load->view('admin/templates/header');
		$this->load->view('admin/index', $data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		// load the create new row form
		$this->load->view('admin/templates/header');
		$this->load->view('admin/create');
		$this->load->view('templates/footer');
	}

	public function sendForm()
	{
		// set the rules for all form input
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[20]|xss_clean|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email|is_unique[users.email]');

		$this->form_validation->set_rules('name_first', 'First Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('name_last', 'Last Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('rel_status', 'Relationship Status', 'trim|required');
		
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		// validate all the input
		if ($this->form_validation->run()) {
			// if validated, insert to database
			$data = array(
				'username'		=> $this->input->post('username'),
				'password'		=> md5($this->input->post('password')),
				'name_first'	=> $this->input->post('name_first'),
				'name_last'		=> $this->input->post('name_last'),
				'email'			=> $this->input->post('email'),
				'relationship_status'	=> $this->input->post('rel_status'),
				'born_place'	=> $this->input->post('born_place'),
				'born_date'	=> $this->input->post('born_date'),
				'occupation'	=> $this->input->post('occupation')
			);

			$result = $this->user_model->insertUser($data);

			// redirect to successful registration page if inserting is successful
			redirect('admin');

		} else {
			// validation failed: reload the form
			$this->load->view('admin/templates/header');
			$this->load->view('admin/create');
			$this->load->view('templates/footer');
		}
		
	}

	public function editPicture($id)
	{
		// get the user pictuere edit
		$data['user'] = $this->user_model->getInfoById($id);
		$data['error'] = '';

		$this->load->view('admin/templates/header');
		$this->load->view('admin/editpicture', $data);
		$this->load->view('templates/footer');
	}

	public function updatePicture()
	{
		// set up the configuration for uploader
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['width']  = '400';
		$config['height']  = '400';
		$config['encrypt_name'] = TRUE;

		// load the library and initialize it with the config attribute
		$this->load->library('upload');
		$this->upload->initialize($config);

		$data = $this->user_model->getInfoById($this->input->post('id'));

		if (!$this->upload->do_upload('photo'))
		{
			$data['error'] = $this->upload->display_errors();

			$this->load->view('admin/templates/header', $data);
			$this->load->view('admin/editpicture', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$photo_data = array('upload_data' => $this->upload->data());
			$this->user_model->updateProfilePicture($data['id'], $photo_data['upload_data']);
			redirect('admin');
		}
	}	

	public function edit($id)
	{
		// get the user
		$data = $this->user_model->getInfoById($id);

		$this->load->view('admin/templates/header');
		$this->load->view('admin/edit', $data);
		$this->load->view('templates/footer');
	}

	public function update()
	{
		$this->load->library('form_validation');
		// set rules for the input
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email');

		$this->form_validation->set_rules('name_first', 'First Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('name_last', 'Last Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('rel_status', 'Relationship Status', 'trim|required');

		// run the validation test
		if ($this->form_validation->run()) {

			// if validated, insert to database
			$data = array(
				'id'			=> $this->input->post('id'),
				'username'		=> $this->input->post('username'),
				'email'			=> $this->input->post('email'),
				'name_first'	=> $this->input->post('name_first'),
				'name_last'		=> $this->input->post('name_last'),
				'relationship_status'	=> $this->input->post('rel_status'),
				'born_place'	=> $this->input->post('born_place'),
				'born_date'		=> date("Y-m-d", strtotime($this->input->post('born_year'). "-" .$this->input->post('born_month'). "-" .$this->input->post('born_date'))),
				'occupation'		=> $this->input->post('occupation')
			);

			$result = $this->user_model->updateUser($data);

			// redirect to successful registration page if inserting is successful

			redirect('admin/update');
		} else {
			// validation failed: reload the form
			$data = $this->user_model->getInfoById($this->input->post('id'));

			$this->load->view('admin/templates/header', $data);
			$this->load->view('admin/edit', $data);
			$this->load->view('templates/footer');
		}
	}

	public function delete($id)
	{
		$result = $this->user_model->deleteUser($id);
		if ($result)
			redirect('/');
	}

}

?>