<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The API Class for the web.
* Implemented with ReST API, returning the response as JSON
*/
class API extends CI_Controller
{
	// main constructor for the API
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// load the api interface
		$this->load->view('api/index');
	}

	public function user_get_info()
	{
		$username = $this->input->post('username');
		// method for the get by username API
		if ($username != NULL) {
			$this->load->model('user_model');
			$result = $this->user_model->getInfoByUsername($username);

			unset($result['password']);

			if (empty($result))
				$result = array('error' => 'Cannot fetch data: Username is Unknown');

			$output = json_encode($result);
			echo $output;
		}

		$this->load->view('api/index');
	}

}

?>