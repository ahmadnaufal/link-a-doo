<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The main class of Home controller
* Handles the user session (cookies), so they will know where to go
*/
class Home extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		if (($sess = $this->session->userdata('loggedin')) == NULL) {
			// if session is not set, redirect to login page
			$this->session->set_flashdata('flash_data', 'Please login first to access this page!');
			redirect('login');	
		}

		$this->load->model('user_model');
	}

	public function index()
	{
		// session set for user, redirect them to the home page
		$sess_data = $this->user_model->getInfoById($this->session->userdata('loggedin')['id']);
		if ($sess_data['is_privileged'] != 1)
			redirect('admin');

		$sess_data['users'] = $this->user_model->getAllUsers();

		$this->load->view('home/templates/header', $sess_data);
		$this->load->view('home/index', $sess_data);
		$this->load->view('templates/footer');
	}

	public function search()
	{
		// get the query from the post if the query is more than one char

		if (strlen($query = $this->input->get('query')) < 2)
			redirect('/');

		$result = $this->user_model->searchUser($query);

		$sess_data = $this->session->userdata('loggedin');
		$sess_data['query'] = $query;

		$sess_data['results'] = $result;

		$this->load->view('home/templates/header', $sess_data);
		$this->load->view('home/search', $sess_data);
		$this->load->view('templates/footer');
	}

	public function logout()
	{
		// unset the userdata session, destroy it
		$this->session->unset_userdata('loggedin');
		$this->session->sess_destroy();
		redirect('login');	// redirect user to login page
	}

}

?>