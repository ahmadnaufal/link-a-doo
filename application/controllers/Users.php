<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The main class of users controller
* Handles the user session (cookies), so they will know where to go
*/
class Users extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		if (($sess = $this->session->userdata('loggedin')) == NULL) {
			// if session is not set, redirect to login page
			$this->session->set_flashdata('flash_data', 'Please login first to access this page!');
			redirect('login');
		}

		if ($sess['is_privileged'] == 0)
			redirect('/');

		$this->load->model('user_model');
	}

	public function index()
	{
		// profile for logged user
		if ($this->session->userdata('loggedin') == NULL) {
			redirect('login');
		} else {
			// fetch the user profile from database with the session id
			$id = $this->session->userdata('loggedin')['id'];
			$data = $this->user_model->getInfoById($id);

			if (empty($data))
				show_404();

			$this->load->view('home/templates/header', $data);
			$this->load->view('user/profile', $data);
			$this->load->view('templates/footer');
		}
	}

	public function settings()
	{
		// user management menu: set all data
		$data = $this->session->userdata('loggedin');
		// load the user model to get content from the database
		$sess_data = $this->user_model->getInfoById($data['id']);

		$this->load->view('home/templates/header', $sess_data);
		$this->load->view('user/settings', $sess_data);
		$this->load->view('templates/footer');
	}

	public function saveConfig() {
		$this->load->library('form_validation');
		// set rules for the input
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email');

		$this->form_validation->set_rules('name_first', 'First Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('name_last', 'Last Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('rel_status', 'Relationship Status', 'trim|required');
		
		$this->form_validation->set_rules('born_place', 'Born Place', 'trim|required');
		$this->form_validation->set_rules('born_date', 'Born Date', 'trim|required|greater_than[0]|less_than[32]');
		$this->form_validation->set_rules('born_month', 'Born Month', 'trim|required|greater_than[0]|less_than[13]');
		$this->form_validation->set_rules('born_year', 'Born Year', 'trim|required|greater_than[1915]');
		$this->form_validation->set_rules('occupation', 'Occupation', 'trim|required');

		// run the validation test
		if ($this->form_validation->run()) {
			// if validated, insert to database
			$data = array(
				'id'			=> $this->input->post('id'),
				'username'		=> $this->input->post('username'),
				'email'			=> $this->input->post('email'),
				'name_first'	=> $this->input->post('name_first'),
				'name_last'		=> $this->input->post('name_last'),
				'relationship_status'	=> $this->input->post('rel_status'),
				'born_place'	=> $this->input->post('born_place'),
				'born_date'		=> date("Y-m-d", strtotime($this->input->post('born_year'). "-" .$this->input->post('born_month'). "-" .$this->input->post('born_date'))),
				'occupation'		=> $this->input->post('occupation')
			);

			$result = $this->user_model->updateUser($data);

			// redirect to successful registration page if inserting is successful

			redirect('users');
		} else {
			// validation failed: reload the form
			$sess_data = $this->user_model->getInfoById($old_data['id']);

			$this->load->view('home/templates/header', $sess_data);
			$this->load->view('user/settings', $sess_data);
			$this->load->view('templates/footer');
		}
	}

	public function profilePictureEdit()
	{
		// user management menu: set all data
		$data = $this->session->userdata('loggedin');
		// load the user model to get content from the database
		$sess_data = $this->user_model->getInfoById($data['id']);
		$sess_data['error'] = '';

		$this->load->view('home/templates/header', $sess_data);
		$this->load->view('user/profilepicture', $sess_data);
		$this->load->view('templates/footer');
	}

	public function updatePicture()
	{
		// set up the configuration for uploader
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['width']  = '400';
		$config['height']  = '400';
		$config['encrypt_name'] = TRUE;

		// load the library and initialize it with the config attribute
		$this->load->library('upload');
		$this->upload->initialize($config);

		$data = $this->session->userdata('loggedin');
		$sess_data = $this->user_model->getInfoById($data['id']);

		if (!$this->upload->do_upload('photo'))
		{
			$sess_data['error'] = $this->upload->display_errors();

			$this->load->view('home/templates/header', $sess_data);
			$this->load->view('user/profilepicture', $sess_data);
			$this->load->view('templates/footer');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->user_model->updateProfilePicture($sess_data['id'], $data['upload_data']);
			redirect('users');
		}
	}

}

?>