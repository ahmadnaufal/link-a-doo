<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The main class of Login controller
* Maintain the login part of the website, including checks, form validation, etc
* Will be used as the main page of the web
*/
class Login extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');	// load the login model
		$this->load->library('form_validation');

		// if the user is already logged in, redir to home page
		if ($this->session->userdata('loggedin') != NULL)
			redirect('home', 'refresh');
	}

	public function index()
	{
		// load the default view of login page
		$this->load->view('templates/header');
		$this->load->view('login/index');
		$this->load->view('templates/footer');
	}

	public function authorize()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		if ($this->form_validation->run()) {
		// initialize the data sent by the form open post method
			$data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password'))
			);

			// data is sent to the model to check if the user is available
			$result = $this->login_model->validateUser($data);
			if ($result->num_rows() == 1) {
				$sess_data = $result->row_array();
				$session = array(
					'id'			=> $sess_data['id'],
					'username' 		=> $sess_data['username'],
					'is_privileged'	=> $sess_data['is_privileged']
				);

				$this->session->set_userdata('loggedin', $session);

				/* Check if the logged user is privileged: DO NEXT */

				redirect('/');
			} else {
				// got no results: the input data is incorrect or unregistered
				redirect('login');
			}
		} else {
			$this->load->view('templates/header');
			$this->load->view('login/index');
			$this->load->view('templates/footer');
		}


	}

}

?>