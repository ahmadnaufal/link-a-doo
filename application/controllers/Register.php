<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The main class of Register controller
* User register, registration form and also validations and inserting users
*/
class Register extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');	// load the register model
		$this->load->library('form_validation');

		/* A condition if user logged in and accessing this page */
	}

	public function index()
	{
		// load the default view of register page
		$this->load->view('templates/header');
		$this->load->view('register/index');
		$this->load->view('templates/footer');
	}

	public function sendForm()
	{
		// set the rules for all form input
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[20]|xss_clean|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email|is_unique[users.email]');

		$this->form_validation->set_rules('name_first', 'First Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('name_last', 'Last Name', 'trim|required|alpha|max_length[20]|xss_clean');
		$this->form_validation->set_rules('rel_status', 'Relationship Status', 'trim|required');
		
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]');

		// validate all the input
		if ($this->form_validation->run()) {
			// if validated, insert to database
			$data = array(
				'username'		=> $this->input->post('username'),
				'password'		=> md5($this->input->post('password')),
				'name_first'	=> $this->input->post('name_first'),
				'name_last'		=> $this->input->post('name_last'),
				'email'			=> $this->input->post('email'),
				'relationship_status'	=> $this->input->post('rel_status')
			);

			$result = $this->user_model->insertUser($data);

			// redirect to successful registration page if inserting is successful
			if (!empty($result))
				$this->load->view('register/formsuccess');
			else
				redirect('register');

		} else {
			$this->load->view('templates/header');
			$this->load->view('register/index');
			$this->load->view('templates/footer');
		}
		
	}

}

?>