<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The main class of Login model
* Maintain the login part of the website, including checks, form validation, etc
* Query for user checking defined here
*/
class Login_model extends CI_Model
{
	
	public function __construct()
	{
		// database loader is autoloaded
		parent::__construct();
	}

	public function validateUser($data)
	{
		/* build a query which selects row with username and password as parameters to WHERE clause */
		$result = $this->db->get_where('users', array(
			'username' => $data['username'],
			'password' => $data['password']
		));

		return $result;
	}

}

?>