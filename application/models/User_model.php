<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* The main class of Home controller
* Handles the user session (cookies), so they will know where to go
*/
class User_model extends CI_Model
{
	
	public function __construct()
	{
		// database loader is autoloaded
		parent::__construct();
	}

	public function insertUser($data)
	{
		/* build a query which inserts the row with username and password as parameters to WHERE clause */
		$result = $this->db->insert('users', $data);

		// since validation is already done by controller, just return the result
		return $result;
	}

	public function getInfoByUsername($username)
	{
		// fetch data from the database with the username
		$result = $this->db->get_where('users', array('username' => $username));

		return $result->row_array();
	}

	public function getInfoById($id)
	{
		// fetch data from the database with the user ID
		$result = $this->db->get_where('users', array('id' => $id));

		return $result->row_array();
	}

	public function getInfoByEmail($email)
	{
		// fetch data from the database with the email
		$result = $this->db->get_where('users', array('email' => $email));

		return $result->row_array();
	}

	public function getAllUsers()
	{
		// get all registered users from the database
		$this->db->order_by('id', 'desc');
		$result = $this->db->get_where('users', array('is_privileged' => 1));

		return $result->result_array();
	}

	public function updateProfilePicture($id, $data)
	{
		// set the path and update the photo name field
		$update = array(
				'photo'	=> $data['file_name']
			);

		$result = $this->db->update('users', $update, array('id' => $id));
		return $result;
	}

	public function updateUser($data)
	{
		// update the value
		$result = $this->db->update('users', $data, array('id' => $data['id']));
		return $result;
	}

	public function deleteUser($id)
	{
		// deleting user from the database
		$result = $this->db->delete('users', array('id' => $id));
		return $result;
	}

	public function searchUser($query)
	{
		// initialize query with keyword
		$this->db->like('username', $query);
		$this->db->or_like('name_first', $query);
		$this->db->or_like('name_last', $query);
		$this->db->or_like('email', $query);

		// execute the search query
		$result = $this->db->get_where('users', array('is_privileged' => 1));
		return $result->result_array();
	}

	public function searchUserAlt($query)
	{
		// alternative search for admin: extended field search
		// initialize query with keyword
		$this->db->like('username', $query);
		$this->db->or_like('name_first', $query);
		$this->db->or_like('name_last', $query);
		$this->db->or_like('email', $query);
		$this->db->or_like('born_place', $query);
		$this->db->or_like('occupation', $query);

		// execute the search query
		$result = $this->db->get_where('users', array('is_privileged' => 1));
		return $result->result_array();
	}

}

?>