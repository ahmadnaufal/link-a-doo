<div class="main-header">
	<div class="container-fluid">
		<div class="row">
			<h1>Search Results</h1>
		</div>
	</div>
</div>

<hr>

<div class="main-content">
	<div class="container-fluid">
		<div class="row">

			<div class="col-md-4">
				<h2>Search results for: '<?= $query ?>'</h2>
			</div>
			<div class="col-md-8">
				<div class="timeline">
					<?php if (!empty($results)) : foreach ($results as $result) : ?>

						<?php
							if ($result['photo'] == NULL)
								$result['photo'] = base_url()."application/assets/img/default.jpg";	
							else
								$result['photo'] = base_url()."uploads/".$result['photo'];
						?>
						<div class="row timeline-item">
							<div class="col-sm-2 col-xs-3 timeline-item-pic">
								<a href="<?= $result['photo'] ?>">
									<img class="img-thumbnail" src="<?= $result['photo'] ?>">
								</a>
							</div>
							<div class="col-sm-10 col-xs-3 timeline-item-info">
								<p><?= $result['name_first']." ".$result['name_last'] ?></p>
								<p><?= $result['username'] ?></p>
								<p><?= $result['email'] ?></p>
							</div>
						</div>

					<?php endforeach; else : ?>
						<h1>No user found..</h1>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
