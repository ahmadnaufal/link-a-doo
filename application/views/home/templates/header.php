<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
	<meta charset="UTF-8">
	
	<title>Link-A-Doo</title>
	<?php echo link_tag('application/assets/css/bootstrap.min.css'); ?>
	<?php echo link_tag('application/assets/css/style.css'); ?>
	<link href="<?= base_url() ?>/application/assets/img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" id="logo" href="<?= base_url() ?>">LinkaDoo</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <form class="navbar-form navbar-left" role="search" method="get" action="<?= base_url() ?>home/search">
	        <div class="form-group">
	          <input type="text" id="search" name="query" class="form-control" placeholder="Search for Users on LinkaDoo!">
	        </div>
	        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
	      </form>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<?= base_url() ?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
	        <li><a href="<?= base_url('u/'.$username) ?>"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
	        <li class="dropdown">
	        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog"></span> Settings <span class="caret"></span></a>
	        	<ul class="dropdown-menu">
			        <li><a href="<?= base_url('users/settings') ?>">Management Menu</a></li>
			        <li><a href="<?= base_url('users/profilepictureedit') ?>">Profile Picture</a></li>
			        <li role="separator" class="divider"></li>
			        <li><a href="<?= base_url('home/logout') ?>"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
		        </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="main">
	<!-- END OF HEADER -->