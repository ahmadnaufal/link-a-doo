<div class="main-header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 col-xs-5 col-sm-offset-1">
				<?php
					if ($photo == NULL)
						$photo = base_url()."application/assets/img/default.jpg";	
					else
						$photo = base_url()."uploads/".$photo;
				?>
				<img src="<?= $photo ?>" class="img-thumbnail">
			</div>
			<div class="col-sm-9 col-xs-7">
				<h1 id="main-header-content">Welcome, <a href="<?= base_url('u/'.$username) ?>"><?= $name_first ?></a></h1>
			</div>
		</div>
	</div>
</div>

<hr>
<div class="main-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<h2>Who are on LinkaDoo?</h2>
			</div>
			<div class="col-md-8">
				<ul class="media-list timeline">
					<?php foreach ($users as $user) : ?>
						<?php if ($user['id'] != $id) : ?>

							<?php
								if ($user['photo'] == NULL)
									$user['photo'] = base_url()."application/assets/img/default.jpg";	
								else
									$user['photo'] = base_url()."uploads/".$user['photo'];
							?>
							<li class="media timeline-item">
								<a href="<?= $user['photo'] ?>" class="pull-left">
									<img class="img-thumbnail img-responsive" src="<?= $user['photo'] ?>">
								</a>
								<div class="media-body timeline-item-info">
									<h4 class="media-heading"><?= $user['name_first']." ".$user['name_last'] ?></h4>
									<p>
										<b>Username: </b><?= $user['username'] ?><br>
										<span class="support-content"><?= $user['email'] ?></span>
									</p>
								</div>
							</li>

					<?php endif; endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</div>
