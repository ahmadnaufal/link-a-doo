<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
	<meta charset="UTF-8">
	
	<title>Link-A-Doo</title>
	<?php echo link_tag('application/assets/css/bootstrap.min.css'); ?>
	<?php echo link_tag('application/assets/css/style.css'); ?>
	<link href="<?= base_url() ?>/application/assets/img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
	
	<div class="front-page">
	<!-- END OF HEADER -->