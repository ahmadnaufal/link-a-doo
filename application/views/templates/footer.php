	<!-- START OF FOOTER, END OF BODY -->
</div>

<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-8" id="left-footer">
				<p><span id="logo">LinkaDoo</span> &copy; 2015</p>
			</div>
			<div class="col-md-6 col-xs-4" id="right-footer">
				<p>
					<a href="<?= base_url() ?>">Home</a> | <a href="#">About</a>
				</p>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript" src="<?php echo base_url();?>application/assets/js/jquery-2.1.4.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>application/assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>application/assets/js/main.js" ></script>

</body>
</html>