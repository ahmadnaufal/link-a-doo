<div class="main-header">
	<div class="container-fluid">
		<div class="row">
			<h1>Edit Your Profile</h1>
		</div>
	</div>
</div>

<hr>
<div class="main-content">
	<div class="container-fluid">
		<?php if (validation_errors()): ?>
			<div class="alert alert-warning"><?= validation_errors(); ?></div>
		<?php endif; ?>

		<?php
			//$attr = array('class' => 'form-inline'); 
			echo form_open('users/saveconfig'/*, $attr*/);
		?>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?= $username ?>">
						<span class="help-block">If you change this, you will have to log in again with your new username.</span>
					</div>
					<input type="hidden" id="id" name="id" value="<?= $id ?>">
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="email" class="form-control" name="email" id="email" placeholder="example@example.com" value="<?= $email ?>">
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="name_first">First Name</label>
						<input type="text" class="form-control" name="name_first" id="name_first" placeholder="First Name" value="<?= $name_first ?>">
					</div>
					<div class="form-group">
						<label for="name_last">Last Name</label>
						<input type="text" class="form-control" name="name_last" id="name_last" placeholder="Last Name" value="<?= $name_last ?>">
					</div>
					<div class="form-group">
						<label for="rel_status">Relationship Status</label>
						<select class="form-control" id="rel_status" name="rel_status" placeholder="Relationship Status" value="<?= $relationship_status ?>">
						   	<option>Single</option>
						    <option>In a Relationship</option>
						    <option>Engaged</option>
						    <option>Married</option>
						    <option>Widowed</option>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						<label for="born_place">Birth City</label>
						<input type="text" class="form-control" id="born_place" name="born_place" placeholder="Birth City" value="<?= $born_place ?>">
					</div>
					<div class="input-group">
						<label for="born-time">Birth Date</label>
						<?php 
							$dtime = strtotime($born_date);
							$year = date("Y", $dtime);
							$month = date("m", $dtime);
							$date = date("d", $dtime);
						?>
						<div id="born-time">
							<div class="col-xs-3">
								<input type="number" class="form-control" id="born_date" name="born_date" placeholder="Date" value="<?= $date ?>">
							</div>
							<div class="col-xs-3">
								<input type="number" class="form-control" id="born_month" name="born_month" placeholder="Month" value="<?= $month ?>">
							</div>
							<div class="col-xs-6">
								<input type="number" class="form-control" id="born_year" name="born_year" placeholder="Year" value="<?= $year ?>">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="occupation">Occupation</label>
						<input type="text" class="form-control" id="occupation" name="occupation" placeholder="Occupation" value="<?= $occupation ?>">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<input type="submit" name="submit" value="Save" class="btn btn-primary pull-right">
			</div>

		</form>
	</div>
</div>