<div class="main-header">
	<div class="container-fluid">
		<div class="row">
			<h1><?= $name_first." ".$name_last; ?></h1>
		</div>
	</div>
</div>

<hr>

<div class="main-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-sm-5">
				<div class="profile-image">
					<?php
						if ($photo == NULL)
							$photo = base_url()."application/assets/img/default.jpg";	
						else
							$photo = base_url()."uploads/".$photo;
					?>
					<a href="<?= $photo ?>">
						<img class="img-responsive img-thumbnail" src="<?= $photo ?>" alt="<?= $name_first." ".$name_last; ?>">
					</a>
				</div>
			</div>
			<div id="profile-data" class="col-md-8 col-sm-7">
				<div class="container-fluid">
					<div class="row">
						<div id="basic-data" class="col-xs-6">
							<p>
								<b>Username</b><br><?= $username ?>
							</p>
							<p>
								<b>E-mail</b><br><?= $email ?>
							</p>
							<p>
								<b>Relationship</b><br><?= $relationship_status; ?>
							</p>
						</div>
						<div id="opt-data" class="col-xs-6">
							<p>
								<b>Birth City</b><br><?= $born_place; ?>
							</p>
							<p>
								<b>Birth Date</b><br><?= $born_date; ?>
							</p>
							<p>
								<b>Occupation</b><br><?= $occupation; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>