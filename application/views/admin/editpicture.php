<div class="main-header">
	<div class="container-fluid">
		<div class="row">
			<h1>Edit Profile Picture</h1>
		</div>
	</div>
</div>

<hr>

<div class="main-content">
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-md-4 col-sm-5">
				<div class="profile-image">
					<?php
						if ($user['photo'] == NULL)
							$user['photo'] = base_url()."application/assets/img/default.jpg";	
						else
							$user['photo'] = base_url()."uploads/".$user['photo'];
					?>
					<a href="<?= $user['photo'] ?>">
						<img class="img-responsive img-thumbnail" src="<?= $user['photo'] ?>" alt="<?= $user['name_first']." ".$user['name_last']; ?>">
					</a>
				</div>
			</div>
			<div class="col-md-8 col-sm-7">
				<?php echo $error; ?>
				<?php echo form_open_multipart('admin/updatepicture'); ?>
					<div class="form-group">
						<input type="file" id="photo" name="photo" required>
					</div>
					<input type="hidden" name="id" value="<?= $user['id'] ?>">
					
					<input type="submit" name="submit" value="Upload" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</div>
