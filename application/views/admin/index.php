<div class="main-header">
	<div class="container-fluid">
		<div class="row">
			<h1><?= $title ?></h1>
		</div>
	</div>
</div>

<hr>

<div class="main-content">
	<div class="container-fluid">
		<div class="row">
			<ul class="media-list timeline">
				<?php foreach ($content as $user) : ?>

					<?php
						if ($user['photo'] == NULL)
							$user['photo'] = base_url()."application/assets/img/default.jpg";	
						else
							$user['photo'] = base_url()."uploads/".$user['photo'];
					?>
					<li class="media timeline-item">
						<a href="<?= base_url('admin/editpicture/'.$user['id']) ?>" class="pull-left">
							<img class="img-thumbnail img-responsive" src="<?= $user['photo'] ?>">
						</a>
						<div class="media-body timeline-item-info">
							<h4 class="media-heading"><?= $user['name_first']." ".$user['name_last'] ?> <span id="username">(<?= $user['username'] ?>)</span></h4>
							<div class="col-xs-5">
								<span class="support-content">
									<b>Email: </b><?= $user['email'] ?></b><br>
									<b>Relationship: </b><?= $user['relationship_status'] ?></b><br>
								</span>
							</div>
							<div class="col-xs-5">
								<span class="support-content">
									<b>Born City: </b><?= $user['born_place'] ?></b><br>
									<b>Born Date: </b><?= $user['born_date'] ?></b><br>
									<b>Occupation: </b><?= $user['occupation'] ?></b><br>
								</span>
							</div>
							<div class="col-xs-2">
								<a href="<?= base_url('admin/edit/'.$user['id']) ?>" id="edit" class="btn btn-warning">Edit</a>
								<a href="<?= base_url('admin/delete/'.$user['id']) ?>" id="delete" onclick="return confirm('Delete this user?');" class="btn btn-danger">Delete</a>
							</div>
						</div>
					</li>

				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>
