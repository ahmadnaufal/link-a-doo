<div class="row">
	<div class="container">
		<div class="panel">
			<!-- THE REGISTER FORM -->
			<?php echo form_open('register/sendForm'); ?>
				<hgroup>
					<a href="<?= base_url() ?>"><span id="logo">LinkaDoo</span></a>
					<p id="caption">Signup now to find your <span id="change">Doodoos</span></p>
				</hgroup>
				<?php if (validation_errors())
					echo '<div class="alert alert-warning">'.validation_errors().'</div>';
				?>
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="username" id="username" placeholder="Username">
					<i class="glyphicon glyphicon-user form-control-feedback"></i>
				</div>
				<div class="form-group has-feedback">
					<input type="email" class="form-control" name="email" id="email" placeholder="example@example.com">
					<i class="glyphicon glyphicon-envelope form-control-feedback"></i>
				</div>

				<hr>
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="name_first" id="name_first" placeholder="First Name">
					<i class="glyphicon glyphicon-user form-control-feedback"></i>
				</div>
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="name_last" id="name_last" placeholder="Last Name">
					<i class="glyphicon glyphicon-user form-control-feedback"></i>
				</div>
				<div class="form-group has-feedback">
					<select class="form-control" id="rel_status" name="rel_status">
					   	<option>Single</option>
					    <option>In a Relationship</option>
					    <option>Engaged</option>
					    <option>Married</option>
					    <option>Widowed</option>
					</select>
					<i class="glyphicon glyphicon-heart-empty form-control-feedback"></i>
				</div>

				<hr>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" name="password" id="password" placeholder="Password">
					<i class="glyphicon glyphicon-lock form-control-feedback"></i>
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="cpassword" id="cpassword" placeholder="Confirm Password">
				</div>
				
				<hr>
				<input type="submit" name="submit" value="Sign Up!" class="btn btn-primary panel-button">
			</form>

			<hr>
			<p id="trail" class="text-center">Already Registered? <a href="<?= base_url('login') ?>">Login Now</a></p>
		</div>
	</div>
</div>