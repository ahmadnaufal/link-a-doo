<div class="row">
 	<div class="container">
		<div class="panel">
		
			<!-- THE LOGIN FORM -->
			<?php echo form_open('login/authorize'); ?>
				<hgroup>
					<a href="<?= base_url() ?>"><span id="logo">LinkaDoo</span></a>
					<p id="caption">Build network for your <span id="change">Doodoos</span></p>
				</hgroup>
				<?php if (validation_errors())
					echo '<div class="alert alert-warning">'.validation_errors().'</div>';
				?>
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="username" id="username" placeholder="Username">
					<i class="glyphicon glyphicon-user form-control-feedback"></i>
				</div>
				<div class="form-group has-feedback">				
					<input type="password" class="form-control" name="password" id="password" placeholder="Password">
					<i class="glyphicon glyphicon-lock form-control-feedback"></i>
				</div>
				<input type="submit" name="submit" value="Log In" class="btn btn-primary panel-button">
			</form>

			<hr>
			<p id="trail">New to Linkadoo? <a href="<?= base_url('register') ?>">Register First!</a></p>

		</div>
	</div>
</div>