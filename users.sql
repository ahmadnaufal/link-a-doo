-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2015 at 05:33 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `linkadoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name_first` varchar(20) NOT NULL,
  `name_last` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `relationship_status` varchar(20) NOT NULL,
  `born_place` varchar(20) DEFAULT NULL,
  `born_date` date DEFAULT NULL,
  `occupation` varchar(20) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `is_privileged` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name_first`, `name_last`, `email`, `relationship_status`, `born_place`, `born_date`, `occupation`, `photo`, `is_privileged`) VALUES
(1, 'ahmadnaufal', 'beb8186ed8ce38f4653a69b1ff491be7', 'Ahmad', 'Farhan', 'guaha@guah.co', 'Single', 'Bandung', '1995-04-03', 'Student', NULL, 1),
(3, 'subli123', '76a158c55e157499bd3a62f281f86c6e', 'Brah', 'Temporary', 'kalexander1@goo.ne', 'Engaged', NULL, NULL, NULL, NULL, 1),
(4, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'Admin', 'admin@linkadoo.com', 'Single', NULL, NULL, NULL, NULL, 0),
(7, 'deni', 'a51202d1732c51a7ce884a9f148100d2', 'Deni', 'Soedendy', 'someah@gmail.com', 'Single', NULL, NULL, NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
