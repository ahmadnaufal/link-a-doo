########
LINKADOO
########

Build network for your Doodoos, Friends, and Partners!
A mini social media designed for simplicity and multiplatform access.

Deployed website: ``http://calonra.tk/ahmad_naufal``

*******************
Deploy Instructions
*******************

1. Clone atau download Repository ini ke dalam folder halaman web server pada hard drive anda, bisa dilakukan menggunakan SourceTree (BitBucket GUI), Website BitBucket, atau menggunakan Git Shell Interface dengan mengetik:

	``git clone https://ahmadnaufal@bitbucket.com/ahmadnaufal/link-a-doo.git``

2. Lakukan konfigurasi pada config.php (``config/config.php``):
	a. Jika anda menggunakan XAMPP atau sejenisnya (Apache Web Server), biarkan baris ``$config['base_url'] = '';``.
	b. Jika anda menggunakan yang lainnya, ganti value ``$config['base_url']`` menjadi URL website tempat deploy.

3. Buat database baru pada database host yang akan digunakan:_
	a. Buka interface database pada host yang akan Anda gunakan, misalnya PHPMyAdmin untuk MySQL.
	b. Buat database baru yang akan digunakan untuk menyimpan tabel web, contohnya database akan diberi nama ``linkadoo``.
	c. Simpan konfigurasi baru tersebut.

3. Lakukan konfigurasi database pada database.php (``config/database.php``):
	a. ``hostname`` adalah lokasi server Database yang akan digunakan sebagai Database utama dari website.
	b. ``username`` adalah Username yang digunakan untuk credentials pada database host yang sebelumnya dimasukkan.
	c. ``password`` aadalah password yang digunakan beserta username untuk credentials pada database host.
	d. ``database`` adalah database yang digunakan untuk web ini. Gunakan database yang sebelumnya telah dibuat pada server, misalnya ``linkadoo``.

4. Import database yang akan digunakan:
	a. Buka database server interface pada host yang akan Anda gunakan, misalnya PHPMyAdmin untuk MySQL.
	b. Buka database yang telah dibuat sebelumnya.
	c. Import file ``linkadoo.sql`` yang terletak di root folder website ini ke dalam database.
	d. Jika import berhasil, silahkan coba buka ``http://localhost/linkadoo`` untuk membuka website yang telah Anda clone sebelumnya.

5. Have fun and get your Doodoos!

*****************
How to Access API
*****************

LinkaDoo API menggunakan ReST API sebagai dasarnya. API digunakan untuk mengambil data user terkait berdasarkan parameter username yang dikirimkan dengan method POST.
API akan mengirimkan response berupa JSON yg berisi data user apabila username ada pada database.

URL: ``/api/user_get_info``
Parameter: username

************
Admin Access
************

Akses admin dapat dilakukan dengan melakukan login pada halaman utama LinkaDoo dengan username ``admin`` dan password ``admin``.

*****************
Development Tools
*****************

1. CodeIgniter 3
2. Bootstrap CSS Framework 3
3. Adobe Photoshop CS6 for Design Purposes
4. Google Color UI Helper

*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

*******
License
*******

Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

*********
Resources
*********

-  `User Guide <http://www.codeigniter.com/docs>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `Community Forums <http://forum.codeigniter.com/>`_
-  `Community Wiki <https://github.com/bcit-ci/CodeIgniter/wiki>`_
-  `Community IRC <http://www.codeigniter.com/irc>`_

Report security issues to our `Security Panel <mailto:security@codeigniter.com>`_, thank you.

***************
Acknowledgement
***************

The CodeIgniter team would like to thank EllisLab, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.